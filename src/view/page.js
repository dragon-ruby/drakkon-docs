import React from "react";
import flux from "@aust/react-flux";
import { Container } from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import Link from "@mui/material/Link";

// Local
import ToolBar from "thing/toolbar";
import Column from "util/column";
import Div from "util/div";
import SideBar from "thing/sidebar";
import Row from "util/row";
import EzText from "util/ez-text";
import EzLink from "util/ez-link";

const Page = ({ status, context }) => {
  const { page, file } = flux.docs.selectState("show", status, context);
  const Output = page;

  // Edit Page Navigation
  const basePath =
    "https://dragon-ruby.gitlab.io/drakkon-docs/-/blob/main/src/docs";
  const editPath = `${basePath}/${file}.js`;

  return (
    <Row style={{ minHeight: 0 }}>
      <SideBar />
      <Column>
        <ToolBar />
        <Div style={{ padding: "1rem", overflow: "auto" }} direction='column'>
          <Container sx={{ position: "relative" }}>
            <Link
              underline='none'
              rel='noopener'
              target='_blank'
              href={editPath}
              sx={{
                position: "absolute",
                right: 0,
                top: 0,
                opacity: 0.8,
                fontSize: "0.8rem",
              }}
            >
              Edit Page
              <EditIcon fontSize='inherit' sx={{ marginLeft: 1 }} />
            </Link>
            {page && <Output context={context} />}
          </Container>
        </Div>

        <Div
          justify='center'
          align='flex-end'
          style={{ paddingBottom: 15 }}
          flex={0}
        >
          <EzText>
            <EzLink href='https://gitlab.com/dragon-ruby/drakkon-docs/-/issues/new'>
              Missing Something? Submit an Issue!
            </EzLink>
          </EzText>
        </Div>
      </Column>
    </Row>
  );
};

export default Page;
