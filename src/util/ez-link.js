import { Link } from "@mui/material";
import React from "react";
import EzText from "./ez-text";

function EzLink({
  href = "http://docs.dragonruby.org/",
  target = "_blank",
  rel = "noreferrer",
  text = null,
  underline = "none",
  children = null,
}) {
  return (
    <EzText component={"span"}>
      <Link href={href} target={target} rel={rel} underline={underline}>
        {text || children || href}
      </Link>
    </EzText>
  );
}

export default EzLink;
