import React from "react";
import flux from "@aust/react-flux";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Link from "@mui/material/Link";
import Switch from "@mui/material/Switch";
import LightModeIcon from "@mui/icons-material/LightMode";
import DarkModeIcon from "@mui/icons-material/DarkMode";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";

// Local
import Div from "util/div";
import AvailableStyles from "util/codes";

// const filterOptions = createFilterOptions({
//   matchFrom: "start",
//   stringify: (option) => option.version,
// });

function ToolBar() {
  const { mode, highlighter } = flux.sys.useState();
  const handleModeChange = () =>
    flux.dispatch("sys/mode", { mode: mode === "dark" ? "light" : "dark" });

  return (
    <AppBar position='static'>
      <Toolbar sx={{ justifyContent: "flex-end" }}>
        <Autocomplete
          disablePortal
          defaultValue={highlighter}
          options={AvailableStyles}
          sx={style.auto}
          size='small'
          disableClearable
          // getOptionLabel={(option) => option.version}
          // filterOptions={filterOptions}
          renderInput={(params) => <TextField {...params} label='Code Style' />}
          onChange={(e, v) => flux.dispatch("sys/highlighter", v)}
          autoHighlight
        />

        <Div flex={0} style={{ marginRight: 2 }} align='center'>
          {mode === "light" && <LightModeIcon fontSize='small' />}
          {mode === "dark" && <DarkModeIcon fontSize='small' />}
          <Switch checked={mode === "dark"} onChange={handleModeChange} />
        </Div>

        <Link {...style.iconBtn} href='https://gitlab.com/dragon-ruby/drakkon'>
          <FontAwesomeIcon color='#2ac2f3' icon={["fab", "gitlab"]} />
        </Link>

        <Link {...style.iconBtn} href='https://dragonruby.org/'>
          <img src={process.env.PUBLIC_URL + "/dragonruby-16x16.png"} alt='' />
        </Link>
      </Toolbar>
    </AppBar>
  );
}

export default ToolBar;

const style = {
  iconBtn: {
    underline: "none",
    rel: "noopener",

    target: "_blank",
    sx: {
      border: "1px solid rgba(255,255,255,0.4)",
      borderRadius: "25%",
      margin: 0.4,
      padding: "0.6rem 0.5rem 0.5rem 0.5rem",
    },
  },

  auto: {
    minWidth: 150,
    marginRight: 2,
    opacity: 0.75,
  },
};
