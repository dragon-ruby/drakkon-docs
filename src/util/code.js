import flux from "@aust/react-flux";
import React, { useEffect, useState } from "react";
import SyntaxHighlighter from "react-syntax-highlighter";
import "react-syntax-highlighter/dist/esm/styles/hljs";
// import { irBlack } from "react-syntax-highlighter/dist/esm/styles/hljs";
import { IconButton } from "@mui/material";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import { trim } from "lodash";

const CopyButton = ({ showCopy, value }) => {
  useEffect(() => {
    setTimeout(() => {
      if (!showCopy) setCopied(false);
    }, 225);
  }, [showCopy]);

  const [copied, setCopied] = useState(false);

  const styles = {
    transition: "all 225ms ease-in-out",
    position: "absolute",
    top: 5,
    right: 5,
    fontSize: "0.5rem",
    opacity: showCopy ? 1 : 0,
  };

  const handleClick = () => {
    setCopied(true);
    navigator.clipboard.writeText(value);
  };

  return (
    <div style={styles}>
      <IconButton onClick={handleClick} sx={{ padding: 1 }} size='small'>
        {!copied && <ContentCopyIcon fontSize='inherit' />}
        {copied && (
          <CheckCircleOutlineIcon fontSize='inherit' color='success' />
        )}
      </IconButton>
    </div>
  );
};

function Code({
  children = null,
  language = "ruby",
  showLineNumbers = true,
  startingLineNumber = true,
}) {
  const highlighter = flux.sys.useState("highlighter");

  const value = trim(children); // Clean Up Code Block
  const [showCopy, setShowCopy] = useState(false);
  const handleEnter = () => setShowCopy(true);
  const handleLeave = () => setShowCopy(false);

  return (
    <div
      style={{ position: "relative" }}
      onMouseEnter={handleEnter}
      onMouseLeave={handleLeave}
    >
      <CopyButton showCopy={showCopy} value={children} />

      <SyntaxHighlighter
        customStyle={{ borderRadius: "0.2rem" }}
        language={language}
        style={
          require(`react-syntax-highlighter/dist/esm/styles/hljs/${highlighter}`)
            .default
        }
        lineNumberStyle={{ color: "rgb(90,90,90)", opacity: 0.5 }}
        startingLineNumber={startingLineNumber}
        showLineNumbers={showLineNumbers}
      >
        {value}
      </SyntaxHighlighter>
    </div>
  );
}

export default Code;
