https://dragon-ruby.gitlab.io/drakkon-docs

# Drakkon Docs

Lots to do:

- Links in the top right
- Versionioning
- Implement a scrollspy like?
- https://github.com/fisshy/react-scroll/tree/master

## Adding Documentation

**Unless you wana get into the ReactJS side of things I would recommend writing/formatting some markdown in an issue for this project!**

### Code

1. Create new file in `src/docs`
2. Do the documentation

### Single Page

```javascript
import flux from "@aust/react-flux";

const Page = () => {
  return <div>Do the Thing!</div>;
};

flux.dispatch("docs/add", {
  name: "About",
  page: Page,
  weight: 10,
  file: "about", // Used for Edit Page
});
```

### Nested

```
flux.dispatch("docs/add", {
  group: "Intro Videos",
  name: "New to Ruby and Programming",
  page: Page,
  weight: 1,
  file: "new-to-ruby-programming", // Used for Edit Page
});

```

3. Create Merge Request
4. Magic

# Other Links

- https://documentation.divio.com/
