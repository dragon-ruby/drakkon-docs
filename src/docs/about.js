import flux from "@aust/react-flux";
import { Box } from "@mui/system";
import Header from "util/header";
import EzText from "util/ez-text";
import EzLink from "util/ez-link";

const Page = () => {
  return (
    <Box>
      <Header size='h3' top={false}>
        Tis a Drakkon!
      </Header>

      <Header>What</Header>

      <EzText>
        Drakkon is a{" "}
        <EzLink href='https://index.rubygems.org/gems/drakkon'>RubyGems</EzLink>{" "}
        CLI tool for working with the DragonRuby Toolkit.
      </EzText>

      <EzText component='span'>
        <ul>
          <li>
            DragonRuby Management (installing / specifying dragonruby versions)
          </li>
          <li>build/publish helpers (copying files etc)</li>
          <li>
            imagemagick helpers (splitting sprites, combining, resizing, color
            alterations)
          </li>
          <li>dynamic requiring, auto load code</li>
          <li>image indexing (pre-process images into a ruby index file)</li>

          <li>
            terminal log / capturing (haven't done much here, but wanted to make
            a CLI and the barebones of the runtime are here)
          </li>
          <li>template deployment (make a template with some input params)</li>
          <li>
            "gem" installation, install / configure / reuse code from other
            sources
          </li>
          <li>
            bundle ruby code into single file (helpful for web stuff or just not
            to have crazy project structure)
          </li>
        </ul>
      </EzText>

      <Header>Why</Header>
      <EzText>
        The original motivation for drakkon was just to collect all of my
        utilities into one place. Then I wanted to have something that did this
        or that. I've been using it myself since my first game jam.
      </EzText>
      <EzText>
        As time goes, I've see a fair amount of other trying to solve or manage
        some of the same things I made this to do. So I decided to share it.
      </EzText>

      <Header>Want to help?</Header>
      <EzText>
        The easiest way is to create issues in this project's issue tracker or
        the drakkon project's issue tracker.
      </EzText>

      <EzText>
        Share ideas, feedback, or write up entire new docs. I'll convert the
        markdown to the react system.
        <EzLink href='https://dragon-ruby.gitlab.io/drakkon-docs/-/issues/new'>
          New Issue
        </EzLink>
      </EzText>

      <Header>Useful Links</Header>

      <EzText>
        <EzLink href='https://gitlab.com/dragon-ruby/drakkon'>
          - Drakkon Project - Ruby Gem
        </EzLink>
      </EzText>
      <EzText>
        <EzLink href='https://gitlab.com/dragon-ruby/drakkon-docs'>
          - Drakkon Docs Project - ReactJs
        </EzLink>
      </EzText>
    </Box>
  );
};

flux.dispatch("docs/add", {
  name: "About",
  page: Page,
  weight: 0,
  file: "about", // Used for Edit Page
});
