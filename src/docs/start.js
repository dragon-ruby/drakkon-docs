import flux from "@aust/react-flux";
import { Box } from "@mui/system";
import Header from "util/header";
import EzText from "util/ez-text";
import Code from "util/code";
import EzLink from "util/ez-link";

const Page = () => {
  return (
    <Box>
      <Header size='h3' top={false}>
        Getting Started
      </Header>

      <EzText>This is the quickstart! The general steps will be:</EzText>
      <EzText>
        <ol>
          <li>Install</li>
          <ul>
            <li>Dependencies</li>
            <li>Drakkon</li>
            <li>DragonRuby</li>
          </ul>

          <li>Setup</li>
          <ul>
            <li>Init</li>
            <li>Image Indexing</li>
            <li>Dynamic Requiring</li>
            <li>etc</li>
          </ul>
          <li>Run DragonRuby with Drakkon</li>
        </ol>
      </EzText>

      <Header>Requirements</Header>
      <EzText>
        <ul>
          <li>Ruby 3+</li>
          <li>DragonRuby</li>
        </ul>
      </EzText>

      <Header>Installation</Header>
      <Code>{`gem install drakkon`}</Code>

      <EzText marginTop={4} bold>
        Ubuntu (22.04)
      </EzText>
      <Code>{`
apt install ruby ruby-dev build-essential

gem install drakkon
      `}</Code>

      <Header>Getting Started with Drakkon</Header>

      <EzText marginTop={2} bold>
        Initializing Drakkon in a new project folder
      </EzText>
      <Code>{`drakkon init`}</Code>

      <EzText marginTop={2} bold>
        The setup command will allow you to configure drakkon functionality such
        as:
      </EzText>

      <EzText>
        <ul>
          <li>What version of DragonRuby to use (from installed list)</li>
          <li>Platforms to Build for</li>
          <li>Image Index</li>
          <li>Manifest</li>
        </ul>
      </EzText>

      <Code>{`drakkon setup`}</Code>

      <Header>Start the game with Drakkon</Header>

      <Code>{`drakkon run`}</Code>

      <Header>Ruby Version Managers</Header>

      <EzText>
        <ul>
          <li>
            <EzLink href='https://github.com/rbenv/rbenv'>rbenv</EzLink>
          </li>
          <li>
            <EzLink href='https://github.com/asdf-vm/asdf-ruby'>asdf</EzLink>
          </li>
          <li>
            <EzLink href='https://rvm.io/'>rvm</EzLink>
          </li>
        </ul>
      </EzText>
    </Box>
  );
};

flux.dispatch("docs/add", {
  name: "Start",
  page: Page,
  weight: 1,
  file: "start", // Used for Edit Page
});
