import flux from "@aust/react-flux";
import history from "history/hash";

function initialSettings() {
  return {
    status: "loading",
    context: "",
    mode: localStorage.getItem("dragonruby/mode") || "dark",
    highlighter: localStorage.getItem("dragonruby/highlighter") || "ir-black",
  };
}

const store = flux.addStore("sys", initialSettings());
store.register("sys/reset", () => initialSettings);

// ========================================================================
// -- History
// ========================================================================
//  Forward/Back Navigation Controls
history.listen(({ location }) => {
  flux.dispatch("sys/update", {
    status: location.pathname.substring(1),
    context: location.state,
  });
});

// Update Handler
store.register("sys/status", async (_, status, context = {}) => {
  history.push(`/${status}`, context);
});

// Update Replace Handler
store.register("sys/replace", async (_, context = {}) => {
  let status = store.selectState("status");
  history.replace(status, context);
});

// ========================================================================
// -- Store Updates
// ========================================================================
store.register("sys/update", async (_, payload) => {
  return (state) => ({ ...state, ...payload });
});

// ========================================================================
// -- Dark/Light Mode
// ========================================================================
store.register("sys/mode", async (dispatch) => {
  const mode = store.selectState("mode");
  const newMode = mode === "dark" ? "light" : "dark";
  localStorage.setItem("dragonruby/mode", newMode);
  dispatch("sys/update", { mode: newMode });
});

// ========================================================================
// -- Highlighter
// ========================================================================
store.register(
  "sys/highlighter",
  async (dispatch, highlighter = "ir-black") => {
    if (highlighter === null) return;
    if (highlighter === "null") return;

    console.log("highlighter", highlighter);
    localStorage.setItem("dragonruby/highlighter", highlighter);
    dispatch("sys/update", { highlighter: highlighter });
  }
);

// ========================================================================
// -- Entrypoint
// ========================================================================
store.register("sys/init", async (dispatch) => {
  let startPath = history.location.pathname.substring(1);
  await dispatch("sys/status", startPath ? startPath : "about");
});
// ========================================================================
// -- Load
// ========================================================================
store.register("sys/start", async (dispatch) => {
  await dispatch("user/meta");
  dispatch("sys/status", "home");
});
