import { Typography } from "@mui/material";
import React from "react";

function EzText({
  children = "default text!",
  fontStyle = null,
  color = "text.primary",
  marginBottom = 1,
  marginTop = 0,
  component = "p",
  fontWeight = "normal",
  bold = false,
  sx = {},
}) {
  sx.fontStyle = fontStyle;
  sx.color = color;
  sx.marginBottom = marginBottom;
  sx.marginTop = marginTop;
  sx.fontWeight = fontWeight;
  if (bold) sx.fontWeight = "bold";

  return (
    <Typography component={component} sx={sx}>
      {children}
    </Typography>
  );
}

export default EzText;
