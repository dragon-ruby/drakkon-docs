import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

// Styles
import "react-activity/dist/Sentry.css";
import "index.css";

// Flux Stores
import "store/sys";
import "store/user";
import "store/alert";
import "store/docs";

// Dynamically Read Docs Files
const importAll = (r) => r.keys().map(r);
importAll(require.context("docs", false, /\.js$/));
// importAll(require.context("generated", false, /\.js$/));

ReactDOM.render(
  <>
    <App />
  </>,
  document.getElementById("root")
);
