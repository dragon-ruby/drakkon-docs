import React from "react";
import flux from "@aust/react-flux";
import { useTheme } from "@mui/material/styles";

// Local
import Page from "view/page";

const Nav = () => {
  const { status, context, mode } = flux.sys.useState();

  const theme = useTheme();

  return (
    <div style={style(theme)}>
      {mode === "dark" && (
        <style jsx='true' global='true'>{`
          ::-webkit-scrollbar {
            width: 10px;
          }

          /* Track */
          ::-webkit-scrollbar-track {
            background: rgb(30, 30, 30);
          }

          /* Handle */
          ::-webkit-scrollbar-thumb {
            background: rgb(70, 70, 70);
          }

          /* Handle on hover */
          ::-webkit-scrollbar-thumb:hover {
            background: #1e8fff;
          }
        `}</style>
      )}

      {status !== "loading" && <Page status={status} context={context} />}
    </div>
  );
};

export default Nav;

const style = (theme) => {
  return {
    display: "flex",
    flex: 1,
    height: "100vh",
    width: "100vw",
    flexDirection: "column",
    backgroundColor: theme.palette.background.paper,
    overflow: "hidden",
  };
};
