import flux from "@aust/react-flux";
import { sortBy, isEmpty, camelCase, uniq, lowerCase } from "lodash";

function initialSettings() {
  return {};
}
const store = flux.addStore("docs", initialSettings());
store.register("docs/reset", () => initialSettings);

// ========================================================================
// -- Doc Add
// ========================================================================
store.register("docs/add", async (dispatch, page) => {
  var pageIdx;

  if (page.group) {
    pageIdx = camelCase(`${page.group}${page.name}`);
  } else {
    pageIdx = camelCase(page.name);
  }

  page.idx = pageIdx;

  dispatch("docs/update", {
    [pageIdx]: page,
  });
});

// ========================================================================
// -- Show Selector
// ========================================================================
store.addSelector("show", (_, idx) => {
  let doc = store.selectState(idx);
  if (isEmpty(doc)) return { name: "Missing!" };
  return doc;
});

// ========================================================================
// -- Index
// ========================================================================
store.addSelector("list", (_, filter = null) => {
  const list = Object.values(store.selectState());
  // Filtering
  if (filter) {
    let filterList = list.filter((entry) => {
      return (
        lowerCase(entry.name).includes(lowerCase(filter)) ||
        lowerCase(entry.group).includes(lowerCase(filter))
      );
    });

    return sortBy(filterList, [(x) => x.weight]);
  }

  const outputList = list.filter((x) => x.group === undefined);
  const groupNames = list
    .filter((x) => x.group !== undefined)
    .map((x) => x.group);

  uniq(groupNames).forEach((name) => {
    let pages = sortBy(
      list.filter((x) => x.group === name),
      [(x) => x.weight]
    );

    outputList.push({
      name: name,
      group: true,
      pages: pages,
      weight: pages[0].weight,
    });
  });

  return sortBy(outputList, [(x) => x.weight]);
});

// ========================================================================
// -- Store Updates
// ========================================================================
store.register("docs/update", async (_, payload) => {
  return (state) => ({ ...state, ...payload });
});
// ========================================================================
